// Dashboard Time
function showTime() {
  let now = new Date();
  let dateFormat = now.toDateString() + " " + now.toLocaleTimeString();
  // console.log(dateFormat)
  const timeElem = document.getElementById('time');

  if (timeElem === null) {
    clearInterval(this.customInterval);
    return
  }
  timeElem.innerHTML = dateFormat
}
const customInterval = setInterval(showTime, 1000);
showTime();