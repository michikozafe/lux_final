// Get List of Rooms
const url = 'http://localhost:5000/api/rooms'
const roomListElem = document.querySelector('#roomList');
fetch(url, {
    method: 'GET',
    headers:{
        'Content-Type': 'application/json'
    },
}).then(res => res.json())
.then(data => {
    let Rooms = ' ';
    data.map(room => {
        // console.log(dev);
        Rooms += 
          `
          <div class="card my-5 bg-gray shadow room-cards">
            <div class="card-body">
              <div class="row" >
                <div class="col-md-5">
                  <img src="https://makitweb.com/demo/broken_image/images/noimage.png" height="280" width="430"/>
                </div>
                <div class="col-md-7">
                  <h3 class="text-uppercase text-crimson font-weight-bold text-center">${room.name}</h3>
                  <hr>
                  <p>${room.description}</p>
                  <p class="mb-0">Floor: ${room.floor}</p>
                  <p>Capacity: ${room.capacity} guests</p>
                  <p>Price: <span class="text-crimson font-weight-bold">&#8369;${room.price}</span> / night</p>
                  <button onclick="handleRoomClick(${room._id})" class="btn btn-crimson btn-block">Book Now</button>
                </div>
              </div>
            </div>
          </div>
          `
    })
    roomListElem.innerHTML = Rooms;
})


