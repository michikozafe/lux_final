// Tooltip
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

// Get List of Rooms
const url = 'http://localhost:5000/api/promotions'
const promosListElem = document.querySelector('#promosList');
fetch(url, {
    method: 'GET',
    headers:{
        'Content-Type': 'application/json'
    },
}).then(res => res.json())
.then(data => {
    let Promos = ' ';
    data.map(promo => {
        const validUntil = new Date(promo.validUntil).toLocaleDateString("en-US")
        Promos += 
          `
          <div class="card my-5 bg-gray shadow room-cards">
            <div class="card-body">
              <div class="row" >
                <div class="col-md-8 mx-auto">
                  <h3 class="text-uppercase text-crimson font-weight-bold text-center">${promo.title}</h3>
                  <hr>
                  <p>${promo.description}</p>
                  <p class="text-crimson font-weight-bold">Valid Until: ${validUntil}</p>
                  <div>
                    <button onclick="handleRoomClick(${promo._id})" class="btn btn-success" data-toggle="tooltip" title="Edit"><i class="fas fa-pencil-alt" ></i></button>
                    <button onclick="handleRoomClick(${promo._id})" class="btn btn-warning" data-toggle="tooltip" title="Delete"><i class="fas fa-trash"></i></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          `
    })
    promosListElem.innerHTML = Promos;
})


